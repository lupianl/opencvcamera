// Nombre:
// Fecha:

#include <opencv2/highgui/highgui.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char* argv[])
{
    CvCapture* capture = cvCaptureFromCAM( CV_CAP_ANY ); // Abre la primera cámara que esté disponible
    if (!capture)
    {
        cerr << "No camera available" << endl; // Si no se pudo abrir la cámara manda error.
        return -1;
    }
    
    IplImage* img;
    
    img = cvQueryFrame( capture );
    
    cvNamedWindow("Image:",1);  // Crea ventana "Image:"
    cvShowImage("Image:",img);  // Despliega imagen en ventana "Image:"

    cvWaitKey();                // Espera a que se presione una tecla
    cvDestroyWindow("Image:");	// Cierra ventana "Image:"

    cvReleaseImage( &img );     // Libera la memoria ocupada por img

    cvReleaseCapture( &capture );
    
    return 0; 
}
